
const DIFF_CMD = 'git diff --name-only master...';
const fs = require('fs');
const INPUT_FILE = '/code/raw_codeclimate.json';
const OUTPUT_FILE = '/code/changed_codeclimate.json';

const changedFiles = process.argv.slice(2);
console.log('changedFiles: ', changedFiles);


const rawdata = fs.readFileSync(INPUT_FILE);
const issues = JSON.parse(rawdata).filter(item => changedFiles.includes(item.location.path));
const data = JSON.stringify(issues);
fs.writeFileSync(OUTPUT_FILE, data);
console.log('issues in changed files:', data);



