
const computeFingerprint = require('../lib/js/compute_fingerprint');
const execute = require('../lib/js/exec');

const CMD = `python3 -m flake8 --exit-zero \\
        --select C901,MC0001,E711,E712,W601,F401,F402,F403,F404,F405,F811,F812,F821,F822,F823,F831,F841,F901,R0123 \\
        --format json --max-complexity 15  `;

const format=(file, issue)=>{
    return `{
        "engine_name":"flake8",
        "severity":"major",
        "check_name":"${issue.code}",
        "description":"https://www.flake8rules.com/rules/${issue.code}.html",
        "categories":["Risk"],
        "type":"issue",
        "fingerprint":"${computeFingerprint(file, ''+ issue.code, '' + issue.line_number)}",
        "location":{
        "lines":{
            "begin":${issue.line_number},
            "end":${issue.line_number},
        },
        "path":"${file}"
        }
    }\0`;
};
const args = process.argv.slice(2);
const dir = args[0] || '/code';

execute(CMD + dir, (json)=>{
    const result = JSON.parse(json);
    Object.keys(result).map((key)=>{
        const file = key;
        const issues = result[key];
        issues.forEach(issue=>{
           console.log(format(file, issue));
        });
    });
});








