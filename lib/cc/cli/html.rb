require "cc/cli/command"
require "cc/analyzer/formatters"
require 'oj'

module CC
  module CLI
    class Html < Command

        def run() 
            generate_html_report(ARGV[1], ARGV[2])
        end
        def generate_html_report(inputFile, outputFile)
            issues = Oj.load(File.read(inputFile))

            formatter = CC::Analyzer::Formatters.resolve('html').new(filesystem)

            formatter.started
            issues.each{|item| formatter.writeHash(item)}
            
            formatter.finished
          ensure
            formatter.close
        end
    end
  end
end