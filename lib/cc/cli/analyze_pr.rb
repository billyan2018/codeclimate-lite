require "cc/cli/analyze"
require 'oj'

module CC
  module CLI
    class AnalyzePr < Analyze


      include CC::Analyzer

      def run
        # Load config here so it sees ./.codeclimate.yml
        load_config()

        # process args after, so it modifies loaded configuration
        process_args
        config.scan_changed_only = true
        bridge = Bridge.new(
          config: config,
          formatter: formatter,
          listener: CompositeContainerListener.new(
            LoggingContainerListener.new(Analyzer.logger),
            RaisingContainerListener.new(EngineFailure),
          ),
          registry: EngineRegistry.new,
        )
        bridge.run

      end

    end
  end
end
