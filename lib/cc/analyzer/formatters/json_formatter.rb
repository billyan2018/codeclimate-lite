require 'oj'
module CC
  module Analyzer
    module Formatters
      class JSONFormatter < Formatter
        def initialize(filesystem)
          @filesystem = filesystem
          @has_begun = false
        end

        def started
          print "["
        end

        def finished
          print "]\n"
        end

        def write(data0)
          # document = Oj.load(data)
          # document["engine_name"] = current_engine.name
          data = StringExtension.strip_or_self!(data0)
          if @has_begun
            print ",\n"
          end
          if !data.include? "engine_name"
            print "{\"engine_name\":\"#{current_engine.name}\"," + data[1,data.length - 1]
          else
            print data
          end
          @has_begun = true
        end

        def failed(output)
          $stderr.puts "\nAnalysis failed with the following output:"
          $stderr.puts output
          exit 1
        end
      end
    end
  end
end
