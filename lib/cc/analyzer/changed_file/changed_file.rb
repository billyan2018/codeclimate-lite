require "cc/analyzer/changed_file/block"
require "git_diff"
module CC
  module Analyzer
    module ChangedFile
      class ChangedFile
        attr_reader :file, :blocks

        def initialize(file)
          @file = file
          @blocks = []
        end

        def append(block)
          if @blocks.length > 0
            last_block = @blocks[-1]
            if last_block.change_end + 1 == block.change_begin
              @blocks[-1] = Block.new(last_block.change_begin, block.change_end)
            else
              @blocks.append(block)
            end
          else
            @blocks.append(block)
          end

        end
        def self.from_git_diff(diff)
          patches = GitDiff.from_string(diff)
          files = patches.files.map { |item|
            file = ChangedFile.new(item.b_path)
            lines = []
            item.hunks.each{ |hunk|
              lines += hunk.lines.map { |line| line.line_number.pair }.filter{|item| item[0] == nil}
            }
            changed_lines = lines.map { |line| CC::Analyzer::ChangedFile::Block.new(line[1], line[1]) }
            changed_lines.each { |block| file.append(block) }
            file
          }
          files
        end
      end
    end
  end
end
