module CC
  module Analyzer
    module ChangedFile
      class Block
        attr_reader :change_begin, :change_end

        def initialize(change_begin, change_end)
          @change_begin = change_begin
          @change_end = change_end
        end
      end
    end
  end
end

