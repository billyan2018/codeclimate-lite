require "securerandom"
require 'oj'
require "string-extension"
require "digest/md5"

include StringExtension

module CC
  module Analyzer
    #
    # Running specifically an Engine container
    #
    # Input:
    #   - name
    #   - metadata
    #     - image
    #     - command (optional)
    #   - config (becomes /config.json)
    #   - label
    #   - io (to write filtered, validated output)
    #
    # Output:
    #   - Container::Result
    #
    class Engine
      Error = Class.new(StandardError)
 
      def initialize(name, metadata, config, label)
        @name = name
        @metadata = metadata
        @config = config
        @label = label.to_s
        @error = nil
      
      end

      def run(io)

        container = Container.new(
          image: metadata.fetch("image"),
          command: metadata["command"],
          name: @name,
          config: @config
        )

        container.on_output("\0") do |output|
          handle_output(container, io, output)
        end

        container.run(container_options).tap do |result|
          result.merge_from_exception(error) if error.present?
        end

      end

      private

      attr_reader :name, :metadata
      attr_accessor :error


      def handle_output(container, io, raw_output0)
        raw_output = StringExtension.strip_or_self!(raw_output0)

        if raw_output.start_with?('[') && raw_output.end_with?(']')
          if raw_output == '[]'
            return
          end
          array = Oj.load(raw_output)
          array.each { |item| handle_output(container, io, item.to_json) }
        else
          output = EngineOutput.new(name, raw_output)
          return if output_filter.filter?(output)
          # unless output.valid?
          #  self.error = Error.new("engine produced invalid output: #{output.error}")
          #  container.stop("output invalid")
          # end
          parsed_output = output.parsed_output


          if parsed_output && inside_changes(parsed_output)
            parsed_output["engine_name"] = @name
            parsed_output["fingerprint"] = compute_fingerprint(parsed_output)
            # unless io.write(Oj.dump(output_overrider.apply(output)))
            unless io.write(Oj.dump(parsed_output))
              self.error = Error.new("#{io.class}#write returned false, indicating an error")
              container.stop("output error")
            end
          end
        end
      end

      def compute_fingerprint(hash)
        md5 = Digest::MD5.new
        md5 << hash["location"]["path"]
        md5 << hash["check_name"]
        md5 << "#{hash['location']['lines']['begin']}"
        md5.hexdigest
      end

      def inside_changes(issue)
        if !@config["scan_changed_only"]
          return true
        end
        if issue["location"] && issue["location"]["path"] \
          && issue["location"]["lines"] \
          && issue["location"]["lines"]["begin"] \
          && issue["location"]["lines"]["end"]
          file = issue["location"]["path"]

          changed_files = @config["changed_files"].filter { |item| item.file == file }
          if changed_files && changed_files.length > 0
            changed_file = changed_files.first
            found = changed_file.blocks.filter { |block|
              block.change_begin <= issue['location']['lines']['begin'] && block.change_end >= issue['location']['lines']['end']
            }
            found && found.length > 0
          else
            false
          end
        else
          false
        end
      end

      # def qualified_name
      #  "#{name}:#{@config.fetch("channel", "stable")}"
      # end

      def container_options
        options = [
          "--cap-drop", "all",
          "--label", "com.codeclimate.label=#{@label}",
          "--log-driver", "none",
          "--memory-swap", "-1",
          "--net", "none",
          "--rm",
          "--volume", "#{code.host_path}:/code:ro",
          # "--volume", "#{config_file.host_path}:/config.json:ro",
          "--user", "9000:9000"
        ]
        if (memory = metadata["memory"]).present?
          options.concat(["--memory", memory.to_s])
        end
        options
      end

      # def container_name

        # @container_name ||= "cc-engines-#{qualified_name.tr(":", "-")}-#{SecureRandom.uuid}"
      # end


      def code
        @code ||= MountedPath.code
      end


      def output_filter
        @output_filter ||= EngineOutputFilter.new(@config)
      end

      def output_overrider
        @output_overrider ||= EngineOutputOverrider.new(@config)
      end
    end
  end
end
