require "fileutils"
require "cc/analyzer/changed_file/changed_file"
module CC
  module Analyzer
    # The shared interface, invoked by Builder or CLI::Analyze
    #
    # Input:
    #   - config
    #     - engines
    #     - exclude_patterns
    #     - development?
    #     - analysis_paths
    #   - formatter
    #     - started
    #     - engine_running
    #     - finished
    #     - close
    #   - listener
    #     - started(engine, details)
    #     - finished(engine, details, result)
    #   - registry
    #
    # Only raises if Listener raises
    #
    class Bridge
      def initialize(config:, formatter:, listener:, registry:)
        @config = config
        @formatter = formatter
        @listener = listener
        @registry = registry
      end
      def run_all_engines(changed_files)
        if @config.scan_changed_only && changed_files.length == 0
          return
        end
        config.engines.filter{|item| item.enabled? }.each do |engine|
          formatter.engine_running(engine) do
            result = nil
            engine_details = nil

            begin
              engine_details = registry.fetch_engine_details(
                engine,
                development: config.development?,
                )
              listener.started(engine, engine_details)
              result = run_engine(engine, engine_details, changed_files)
            rescue CC::EngineRegistry::EngineDetailsNotFoundError => ex
              result = Container::Result.skipped(ex)
            end

            listener.finished(engine, engine_details, result)
            result
          end
        end
      end
      def run
        changed_files = [] 
        if @config.scan_changed_only 
          changed_files = detect_changed_files
        end
        formatter.started
        run_all_engines(changed_files)
        formatter.finished
      ensure
        formatter.close
      end

      private

      attr_reader :config, :formatter, :listener, :registry

      def run_engine(engine, engine_details, changed_files)
        # Analyzer::Engine doesn't have the best interface, but we're limiting
        # our refactors for now.
        Engine.new(
          engine.name,
          {
            "image" => engine_details.image,
            "command" => engine_details.command,
            "memory" => engine_details.memory,
          },
          build_config(engine, changed_files),
          engine.container_label,
        ).run(formatter)
      end

      def build_config(engine, changed_files)

        engine.config.merge(
          "channel" => engine.channel,
          "include_paths" => engine_workspace(engine).paths,
          "changed_files" => changed_files,
          "scan_changed_only" => @config.scan_changed_only
        )
      end

      def detect_changed_files
        git_diff_output = `git diff #{@config.target_branch}...`
        ChangedFile::ChangedFile.from_git_diff(git_diff_output)
        # return changes.map { |patch| patch.file }
        # changed_files_output.split("\n").select { |file| File.file?(file) }
      end

      def engine_workspace(engine)
        if engine.exclude_patterns.any?
          workspace.clone.tap do |engine_workspace|
            engine_workspace.remove(engine.exclude_patterns)
          end
        else
          workspace
        end
      end

      def workspace
        @workspace ||= Workspace.new.tap do |workspace|
          workspace.add(config.analysis_paths)
          unless config.analysis_paths.any?
            workspace.remove([".git"])
            workspace.remove(config.exclude_patterns)
          end
        end
      end
    end
  end
end
