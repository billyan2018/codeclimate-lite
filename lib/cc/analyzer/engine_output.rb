require 'oj'
module CC
  module Analyzer
    class EngineOutput
      @@root = `readlink -f /code`.strip! # find the physical root path
      delegate :blank?, to: :raw_output
      attr_reader :parsed_output

      def initialize(name, raw_output)
        @name = name
        @raw_output = raw_output
        parse(raw_output)
      end

      def issue?
        valid_with_type?("issue")
      end

      def measurement?
        valid_with_type?("measurement")
      end

      def as_issue
        Issue.new(name, raw_output)
      end

      def to_json
        if issue?
          as_issue.to_json
        elsif measurement?
          Measurement.new(name, raw_output).to_json
        end
      end

      def valid?
        valid_json? && validator && validator.valid?
      end

      def error
        if !valid_json?
          { message: "Invalid JSON", output: raw_output }
        elsif !validator.present?
          { message: "Unsupported document type", output: raw_output }
        else
          validator.error
        end
      end

      def parse(text)
        parsed_obj = Oj.load(text)
        if parsed_obj
          if parsed_obj["location"] && parsed_obj["location"]["path"]
            parsed_obj["location"]["path"] = format_path(parsed_obj["location"]["path"])
          end
          # same as https://github.com/codeclimate/codeclimate/blob/master/lib/cc/analyzer/issue.rb#L57
          # but we are not parsing into the issue type
          if parsed_obj["severity"] == IssueValidations::SeverityValidation::NORMAL
            parsed_obj["severity"] = IssueValidations::SeverityValidation::MINOR
          end
          parsed_obj.delete("content")
        end
        @parsed_output = parsed_obj
      rescue Oj::ParseError
        @parsed_output = nil
      end

      def trim_head(text, head)
        if text.start_with?(head)
          return text[head.length, text.length - 1]
        end
        return text
      end

      def format_path(path)
        formatted = trim_head(path, "/code")
        formatted = trim_head(formatted, @@root)
        formatted = trim_head(formatted, "/")
        return formatted
      end

      private

      attr_accessor :name, :raw_output

      def valid_json?
        parsed_output.present?
      end

      def valid_with_type?(type)
        parsed_output &&
          parsed_output["type"].present? &&
          parsed_output["type"].downcase == type
      end



      def validator
        if issue?
          IssueValidator.new(parsed_output)
        elsif measurement?
          MeasurementValidator.new(parsed_output)
        end
      end
    end
  end
end
