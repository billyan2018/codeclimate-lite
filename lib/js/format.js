const safeString = (str) => str.replace(/\\"/g, '\'');
module.exports = safeString;