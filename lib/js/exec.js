const exec = require('child_process').exec;
const execute = (command, callback)=>{
    exec(command, function(error, stdout, stderr){
        callback(stdout);
    });
};
module.exports = execute;