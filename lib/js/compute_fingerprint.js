const crypto = require("crypto");

const Fingerprint = function (path, rule, line) {
  this.path = path;
  this.rule = rule;
  this.line = line;
};

Fingerprint.prototype.compute = function() {
 

 // if (this.rule === "complexity") {
    const md5 = crypto.createHash("md5");
    md5.update(this.path);
    md5.update(this.rule);
    md5.update(this.line);

    return md5.digest("hex");

};


function computeFingerprint(path, rule, line) {

  return new Fingerprint(path, rule, line).compute();
}

module.exports = computeFingerprint;
