module StringExtension
    def self.strip_or_self!(str)
      str.strip! || str if str
    end
end
