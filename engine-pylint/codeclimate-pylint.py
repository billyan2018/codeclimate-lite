#!/usr/bin/env python3

import glob
import json
import os.path
import sys

from pylint.lint import Run

from reporter import CodeClimateReporter


include_paths = ['/code/']
if (len(sys.argv) >= 2):
    include_paths = sys.argv[1].split(',')


parameters = []
for path in include_paths:
    if os.path.isdir(path):
        parameters += glob.glob(path + "*/**/*.py", recursive=True)
    elif path.endswith('.py'):
        parameters.append(path)

if (len(parameters) > 0):
    # disable and enable rules
    # parameters.append('--disable=all') 
    # parameters.append('--enable=C0111,C0112,C0403,C0401,C0402,R0123,R1704') 
    # or just use a config file
    parameters.append('--rcfile=/usr/src/app/rules/.pylintrc')  # config file
    Run(parameters, reporter=CodeClimateReporter(), do_exit=False)
 