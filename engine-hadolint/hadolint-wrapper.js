const computeFingerprint = require('../lib/js/compute_fingerprint.js');
const fs = require('fs');
const path = require('path');
const execute = require('../lib/js/exec');
const safeString = require('../lib/js/format');

const CMD = 'hadolint --config /usr/src/app/rules/.hadolint.yaml -f json ';

const runCheck=(files)=>{
    execute(CMD + ' ' + files, (json)=>{
       const parsed = JSON.parse(json);
       parsed.forEach(element => {
           console.log(format(element));
       });
    });
};

const format=(issue)=>{
    return `{
        "severity":"major",
        "check_name":"${issue.code}",
        "description":"${safeString(issue.message)} https://github.com/hadolint/hadolint/wiki/${issue.code}",
        "categories":[
        "Bug Risk"
        ],
        "type":"issue",
        "fingerprint":"${computeFingerprint(issue.file, ''+ issue.code, '' + issue.line)}",
        "location":{
        "lines":{
        "begin":${issue.line},
        "end":${issue.line}
        },
        "path":"${issue.file}"
        }
    }\0`;
};


const walk = (dir, done)=>{
    const changedFiles = process.argv.slice(2)[0];
    if (changedFiles && changedFiles.length > 0) {
        const paths = changedFiles.split(',');
        const shFiles = paths.filter(file => file.endsWith('Dockerfile'));
        done(null, shFiles);
        return;
    }
  let results = [];
  fs.readdir(dir, (err, list)=> {
    if (err) return done(err);
    let pending = list.length;
    if (!pending) return done(null, results);
    list.forEach((file)=>{
      file = path.resolve(dir, file);
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function(err, res) {
            results = results.concat(res);
            if (!--pending) done(null, results);
          });
        } else {
          if (file.endsWith('Dockerfile')){
            results.push(file);
          }
          if (!--pending) done(null, results);
        }
      });
    });
  });
};

walk('/code', (err, results)=>{
    if (err) throw err;
    const chunk = 5;
    let i,j;
    // slice all files into chunks
    for (i = 0,j = results.length; i < j; i += chunk) {
        const temporary = results.slice(i, i + chunk);
        const allFiles = temporary.join(' ')
        runCheck(allFiles);
    }
});
