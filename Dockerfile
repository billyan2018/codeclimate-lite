FROM ruby:2.6.6-alpine
ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk/jre
ENV PATH $PATH:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin

ENV JAVA_VERSION 8u275
ENV JAVA_ALPINE_VERSION 8.275.01-r0
ENV BUNDLER_VERSION 2.0.2

ENV LANG en_US.UTF-8

USER root

RUN adduser -u 9000 -D app

RUN apk add --no-cache ca-certificates curl jq bash  make g++ git && \
    update-ca-certificates
## copy all files
COPY . /usr/src/app

## ==== shellcheck
## glibc
# COPY engine-shellcheck/docker/files/ /shellcheck   
# COPY engine-shellcheck/sgerrand.rsa.pub  /etc/apk/keys/sgerrand.rsa.pub
# WORKDIR /usr/src/app/engine-shellcheck/glibc
# RUN apk add glibc-2.33-r0.apk glibc-bin-2.33-r0.apk glibc-i18n-2.33-r0.apk \
# && /usr/glibc-compat/bin/localedef -i en_US -f UTF-8 en_US.UTF-8
# WORKDIR /usr/src/app/engine-shellcheck

# COPY engine-shellcheck/src /usr/src/app/shellcheck/src
# COPY engine-shellcheck/LICENSE /usr/src/app/shellcheck/LICENSE
# COPY engine-shellcheck/codeclimate-shellcheck /usr/src/app/shellcheck/engine
# COPY engine-shellcheck/data/env.yml /usr/src/app/shellcheck/env.yml
## ===== hadolint
WORKDIR /usr/src/app/engine-hadolint
RUN  curl -fsSLO   https://github.com/hadolint/hadolint/releases/download/v2.6.0/hadolint-Linux-x86_64  \
 && mv hadolint-Linux-x86_64 /bin/hadolint \
 && chmod +x /bin/hadolint
## ===== Checkstyle
WORKDIR /usr/src/app/engine-checkstyle
RUN { \
		echo '#!/bin/sh'; \
		echo 'set -e'; \
		echo; \
		echo 'dirname "$(dirname "$(readlink -f "$(which javac || which java)")")"'; \
	} > /usr/local/bin/docker-java-home \
	&& chmod +x /usr/local/bin/docker-java-home \
    && set -x \
    && apk add --no-cache --update \
    		openjdk8-jre="$JAVA_ALPINE_VERSION" \
    && [ "$JAVA_HOME" = "$(docker-java-home)" ] \
    && ./bin/install-checkstyle.sh

## ===== PMD
WORKDIR /usr/src/app/engine-pmd
RUN /usr/src/app/engine-pmd/bin/install-pmd.sh

## ===== ESlint

### Install
WORKDIR /usr/src/app/engine-eslint

ENV PREFIX=/usr/local/node_modules
ENV PATH=$PREFIX/.bin:$PATH
ENV NODE_PATH=$PREFIX
ENV NPM_CONFIG_PREFIX=$PREFIX

RUN mkdir $PREFIX

# COPY /engine-eslint/bin/docs ./bin/docs
# COPY /engine-eslint/engine.json /engine-eslint/package.json /engine-eslint/yarn.lock ./

### Install nodejs, borrowed from https://github.com/nodejs/docker-node/blob/main/12/alpine3.11/Dockerfile#L21
RUN  NODE_VERSION=12.22.7 \
     && ARCH='x64' \
     && curl -fsSLO --compressed "https://unofficial-builds.nodejs.org/download/release/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" \
      && tar -xJf "node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
      && ln -s /usr/local/bin/node /usr/local/bin/nodejs
### Install eslint
RUN set -eux \
    & apk add --no-cache yarn && \
    yarn config set prefix $PREFIX && \
    yarn install --modules-folder $PREFIX && \
    yarn cache clean && \
    chown -R app:app $PREFIX  &&\
    # version="v$(yarn list eslint --depth=0 | grep eslint | sed -n 's/.*@//p')" && \
    # bin/docs "$version" && \
    # cat /usr/src/app/engine-eslint/engine.json | jq ".version = \"$version\"" > /engine.json && \
    chmod +x /usr/src/app/engine-eslint/bin/eslint.js && \
    cp /usr/src/app/rules/.eslintrc.yaml /usr/src/app/engine-eslint/.eslintrc.yaml


## pylint
WORKDIR /usr/src/app/engine-pylint
RUN apk add --no-cache python3 python3-dev py3-pip \
&& python3 -m pip install --no-cache-dir  -r requirements.txt

## flake8
# RUN python3 -m pip install flake8-codeclimate
WORKDIR /usr/src/app/engine-flake8
# RUN python3 setup.py install
RUN python3 -m pip install --no-cache-dir flake8 flake8-json

## codeclimate

WORKDIR /usr/src/app

ENV CODECLIMATE_DOCKER=1 BUNDLE_SILENCE_ROOT_WARNING=1

RUN  apk --no-cache  add \
      build-base \
      openssh-client \
      openssl \
      ruby-bigdecimal \
      ruby-bundler \
      ruby-dev \
      && \
      gem install bundler:$BUNDLER_VERSION && \
      bundle install -j 4 && \
      apk del build-base && \
      rm -fr /usr/share/ri && \
      chmod +x /usr/src/app/bin/codeclimate

## Finish
ENTRYPOINT ["/usr/src/app/bin/codeclimate"]
