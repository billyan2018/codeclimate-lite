require "digest/md5"
require "json"
require "posix/spawn"
require "nokogiri"
require "yaml"

module CC
  module Engine
    class Checkstyle

      EXTENSIONS = %w[.java].freeze
      DEFAULT_CATEGORIES = ["annotation","blocks","design","coding","header","imports",
      "javadoc","metrics","modifier","naming","regexp","sizes","whitespace"].freeze

      def initialize(files_or_dir_to_scan, io)
        @files_or_dir_to_scan = files_or_dir_to_scan
        @io = io
      end

      def run
        return if include_paths.length == 0


        pid, _, out, err = POSIX::Spawn.popen4(command_string)

        xml = out.read

        issues = Nokogiri::XML(xml).xpath("//file//error")

        issues.each do |issue|
          print_issue(issue)
        end
      ensure
        if pid
          STDERR.print err.read
          [out, err].each(&:close)
          Process::waitpid(pid)
        end
      end

      private

      attr_reader :io, :files_or_dir_to_scan

      def include_paths
        files = files_or_dir_to_scan.split(",")
          .map{|item| item.strip}
          .select{|item| EXTENSIONS.include?(File.extname(item)) || item.end_with?("/")}
        files.join(" ")

      end


      def command_string
        "java -Xmx1024m -jar /usr/local/bin/checkstyle.jar -c /usr/src/app/rules/checkstyle.xml -f xml #{include_paths}"
      end

      def format_check_name(issue_name)
        issue_name.split(".").last.split(/(?=[A-Z])/).join(" ")
      end

      def format_path(path)
        formatted = path.gsub("/code","")
        if formatted.start_with? "/"
          formatted[1, formatted.length - 1]
        end
      end

      def format_severity(severity)
        case severity
        when "info"
          "info"
        when "warning"
          "minor"
        when "error"
          "critical"
        end
      end

      def print_issue(issue)
        source = issue.attributes["source"].value
        check_name = "com.puppycrawl.tools.checkstyle.checks.indentation.IndentationCheck"
        array = check_name.split(".")
        category = array[array.length-2]
        rule_name = array.last[0...-5]
        category = DEFAULT_CATEGORIES.include?(category)? category: "misc"
        message = issue.attributes["message"].value
        desc_text = " " + message +  " https://checkstyle.sourceforge.io/config_" + category+".html#" + rule_name
        issue_hash = {
          categories: ["Style"],
          check_name: rule_name,
          engine_name: "checkstyle",
          description: desc_text,
          location: {
            lines: {
              begin: issue.attributes["line"].value.to_i,
              end: issue.attributes["line"].value.to_i,
            },
            path: format_path(issue.parent.attributes["name"].value),
          },
          type: "issue",
          remediation_points: 100_000,
          severity: format_severity(issue.attributes["severity"].value),
        }
        io.print "#{issue_hash.to_json}\0"
      end

    end
  end
end
