#!/usr/bin/env bash
set -euo pipefail

LIB_DIR=/usr/src/app/engine-pmd/lib

download_pmd() {
  URL="https://github.com/pmd/pmd/releases/download/pmd_releases/6.40.0/pmd-bin-6.40.0.zip"
  wget -O pmd.zip $URL
}

install_pmd() {
  unzip pmd.zip
  mv ${LIB_DIR}/pmd-bin*/* ${LIB_DIR}/pmd
}

cleanup() {
  rmdir ${LIB_DIR}/pmd-bin*
  rm pmd.zip
}

cd ${LIB_DIR}
rm -rf *
mkdir -p ${LIB_DIR}/pmd

download_pmd
install_pmd
cleanup
