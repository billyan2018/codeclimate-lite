const fs = require('fs');
const execute = require('../lib/js/exec');
const rules = JSON.parse(fs.readFileSync('/usr/src/app/engine-pmd/java-rules.json', 'utf8'));

const handleIssue = (input) => {
  let issue;
  try {
    issue = JSON.parse(input);
  } catch {
    // ignore siliently
  }

  if (issue == null) {
    return;
  }
  delete issue['content'];
  issue['engine_name'] = 'pmd';
  const ruleInMapping = rules.find(element => element['name'] === issue['check_name']);
  if (ruleInMapping ) {
    const docUrl = ruleInMapping['url'];
    issue['description'] = `${issue['description']} https://pmd.github.io/latest${docUrl} `;
    console.log(JSON.stringify(issue) + '\u0000\n');
  }
};
let temp = '';
const handleData = (text, handler) => {
  const position = text.indexOf('\0');
  if (position >= 0) {
    temp += text.substring(0, position);
    handler(temp);
    temp = '';
    if (position + 1 < text.length) {
      handleData(text.substring(position + 1), handler);
    }
  } else {
    temp += text;
  }
  if (temp.length > 0) {
    handler(temp);
    temp = '';
  }
};

const PMD_CMD = '/usr/src/app/engine-pmd/lib/pmd/bin/run.sh pmd -cache /tmp/pmd-cache  -f codeclimate -R /usr/src/app/rules/ruleset.xml -failOnViolation false '
+ process.argv.slice(2).join(' ');
console.log(PMD_CMD);
execute(PMD_CMD, (output)=>{
  handleData(output, handleIssue);
});
