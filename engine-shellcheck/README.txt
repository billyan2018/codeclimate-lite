This is a precompiled ShellCheck binary.
      https://www.shellcheck.net/

ShellCheck is a static analysis tool for shell scripts.
It's licensed under the GNU General Public License v3.0.
Information and source code is available on the website.

This binary was compiled on Mon Apr 19 22:37:04 UTC 2021.



      ====== Latest commits ======

commit cff3e22911f25283ccef0a23bdfdfaafe3ad7c40
Author: Vidar Holen <spam@vidarholen.net>
Date:   Mon Apr 19 14:31:13 2021 -0700

    Stable version v0.7.2
    
    This release is dedicated to ethanol, for keeping
    COVID-19 off both our hands and our minds.
