require "spec_helper"


describe 'StringExtension' do
    it "strip_or_self" do
      expect(StringExtension.strip_or_self!("abc")).to eq "abc"
    end
end
