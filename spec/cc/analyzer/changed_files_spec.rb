require "spec_helper"
require "cc/analyzer/changed_file/changed_file"
require "cc/analyzer/changed_file/block"




describe CC::Analyzer::ChangedFile::ChangedFile do
  describe "#parse" do
    it "parse git diff with 1 line change" do
      diff = <<'EOL'
diff --git a/lib/saddler/reporter/github.rb b/lib/saddler/reporter/github.rb
index fc33cdd..0638356 100644
--- a/lib/saddler/reporter/github.rb
+++ b/lib/saddler/reporter/github.rb
@@ -2,6 +2,7 @@
 require 'octokit'
 require 'git'
 require 'saddler/reporter/github/version'
+require 'saddler/reporter/github/support'
 require 'saddler/reporter/github/helper'
 require 'saddler/reporter/github/client'
 require 'saddler/reporter/github/comment'
EOL

      files = CC::Analyzer::ChangedFile::ChangedFile.from_git_diff(diff)
      expect(files.length).to eq 1
      expect(files[0].file).to eq "lib/saddler/reporter/github.rb"
      expect(files[0].blocks.length).to eq 1
      expect(files[0].blocks[0].change_begin).to eq 5
      expect(files[0].blocks[0].change_end).to eq 5
    end

    it "parse git diff with multiple lines change" do
      diff = <<'EOL'
diff --git a/.codeclimate.yml b/.codeclimate.yml
index c3d40c0..fb1539c 100644
--- a/.codeclimate.yml
+++ b/.codeclimate.yml
@@ -1,5 +1,5 @@
 version: "2"         # required to adjust maintainability checks
-plugins:
+plugins:
   structure:
     enabled: false
   duplication:
@@ -7,24 +7,17 @@ plugins:
   sonar-java:
     enabled: false
   pmd:
-    enabled: true
-    config:
-      file: rules/ruleset.xml
+    enabled: false
   checkstyle:
-    enabled: true
-    config:
-      file: "rules/checkstyle.xml"
-  shellcheck:
-    enabled: true
+    enabled: false
   eslint:
     enabled: true
-    channel: "eslint-6"
-    config:
-      config: .eslintrc.yaml
-  pep8:
+  pylint:
+    enabled: false
+  flake8:
+    enabled: false
+  hadolint:
     enabled: true
-  # pylint:
-  #  enabled: true
 exclude_paths:
   - "**/build"
   - "**/node_modules/"
EOL
      files = CC::Analyzer::ChangedFile::ChangedFile.from_git_diff(diff)
      p "=============================="
      p files
      p "=============================="
      expect(files.length).to eq 1
      expect(files[0].file).to eq ".codeclimate.yml"
      expect(files[0].blocks.length).to eq 4
      expect(files[0].blocks[0].change_begin).to eq 2
      expect(files[0].blocks[0].change_end).to eq 2
      expect(files[0].blocks[1].change_begin).to eq 10
      expect(files[0].blocks[1].change_end).to eq 10
    end

  end
end