module.exports = {
  extends: ['./eslint-config-frontier-react.js'],
  plugins: ['prettier'],
  ignorePatterns: [
    "fle-b.js"
  ],
};
