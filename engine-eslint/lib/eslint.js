const fs = require('fs');
const glob = require('glob');

const { promisify } = require('util');
const exec = promisify(require('child_process').exec);

const patch = require('./eslint6-patch')();
const BatchSanitizer = require('./batch_sanitizer');
const checks = require('./checks');
const computeFingerprint = require('../../lib/js/compute_fingerprint.js');

const CLIEngine = patch.eslint.CLIEngine;
const options = {
  extensions: ['.js', '.ts'],
  ignore: true,
  reset: false,
  useEslintrc: true,
  baseConfig: {}
};
const excludedPaths = ['/node_modules/', '/dist/', '/build/', '/lib/', 'vendor'];

function trimPath(path, toTrim) {
  if (path.startsWith(toTrim)) {
    return path.substring(toTrim.length);
  }
  return path;
}

async function run(console, runOptions) {
  const STDOUT = console.log;
  console.log = console.error;


  var cli; // instantiation delayed until after options are (potentially) modified
  var debug = false;
  var ignoreWarnings = false;
  var sanitizeBatch = true;
  var ESLINT_WARNING_SEVERITY = 1;

  // a wrapper for emitting perf timing
  function runWithTiming(name, fn) {
    const start = new Date();
    const result = fn();

    if (debug) {
      const duration = (new Date() - start) / 1000;
      console.error('eslint.timing.' + name + ': ' + duration + 's');
    }

    return result;
  }

  function retrieveSeverity(level) {
    /* https://eslint.org/docs/user-guide/configuring/rules
    "off" or 0 - turn the rule off
    "warn" or 1 - turn the rule on as a warning (doesn't affect exit code)
    "error" or 2 - turn the rule on as an error (exit code is 1 when triggered)
    */
    if (typeof level === 'number' && level >= 2) {
        return 'critical';
    } else {
        return 'major';
    }
  }

  function buildIssueJson(message, path) {
    // ESLint doesn't emit a ruleId in the
    // case of a fatal error (such as an invalid
    // token)
    let checkName = message.ruleId;
    if (message.fatal) {
      checkName = 'fatal';
    }
    const line = message.line || 1;

    const issue = {
      type: 'issue',
      categories: checks.categories(checkName),
      check_name: checkName,
      description: `${message.message}  https://eslint.org/docs/rules/${checkName}`,
      fingerprint: computeFingerprint(path, checkName, message.message),
      severity: retrieveSeverity(message.severity),
      location: {
        path: path,
        lines: {
          begin: line,
          end: line,
        },
      },
      remediation_points: checks.remediationPoints(checkName, message, cli.getConfigForFile(path)),
    };
    return JSON.stringify(issue);
  }

  function isFileWithMatchingExtension(file, extensions) {
    if (file.indexOf('.min.js') > 0) {  // not to scan minified files
      return false;
    }
    const stats = fs.lstatSync(file);

    const extension = '.' + file.split('.').pop();
    return stats.isFile() && !stats.isSymbolicLink() && extensions.indexOf(extension) >= 0
  }

  function isFileIgnoredByLibrary(file) {
    return cli.isPathIgnored(file);
  }

  function inclusionBasedFileListBuilder(includePaths) {
    // Uses glob to expand the files and directories in includePaths, filtering
    // down to match the list of desired extensions.
    return function (extensions) {
      const analysisFiles = [];
      includePaths.forEach((fileOrDirectory) => {
        if (/\/$/.test(fileOrDirectory)) {
          // if it ends in a slash, expand and push
          const filesInThisDirectory = glob.sync(fileOrDirectory + '/**/**');
          filesInThisDirectory.forEach(file => {
            if (!isFileIgnoredByLibrary(file)
            && isFileWithMatchingExtension(file, extensions)
            && !excludedPaths.some(item=>file.indexOf(item) > 0)
            ) {
              analysisFiles.push(file);
            }
          });
        } else {
          if (
            !isFileIgnoredByLibrary(fileOrDirectory) &&
            isFileWithMatchingExtension(fileOrDirectory, extensions)
          ) {
            analysisFiles.push(fileOrDirectory)
          }
        }
      });
      return analysisFiles
    }
  }

  function overrideOptions(userConfig) {
    // eslint don't like the config file outside the folder, so copy it in dockerfile
    options.configFile = '/usr/src/app/engine-eslint/.eslintrc.yaml';
    options.useEslintrc = false;

    if (userConfig.extensions) {
      options.extensions = userConfig.extensions
    }

    if (userConfig.ignorePath) {
      options.ignorePath = userConfig.ignorePath
    }

    ignoreWarnings = userConfig.ignoreWarnings
    debug = userConfig.debug
    sanitizeBatch = userConfig.sanitizeBatch
  }

  async function analyzeFiles(analysisFiles) {
    let batchNum = 0;
    let batchSize = 10;
    let batchFiles;
    let batchReport;

    while (analysisFiles.length > 0) {
      batchFiles = analysisFiles.splice(0, batchSize);
      if (sanitizeBatch) {
        batchFiles = new BatchSanitizer(batchFiles).sanitizedFiles();
      }

      if (debug) {
        console.error('Analyzing: ' + batchFiles);
      }

      runWithTiming('analyze-batch-' + batchNum, function () {
        batchReport = cli.executeOnFiles(batchFiles);
      });

      let physicalPath = await exec('readlink -f /code');
      physicalPath = physicalPath.stdout.trimEnd();
      batchReport.results.forEach(function (result) {
        let path = result.filePath;
        path = trimPath(path, '/code/');
        path = trimPath(path, physicalPath);
        path = trimPath(path, '/');
        

        result.messages.forEach(function (message) {
          if (ignoreWarnings && message.severity === ESLINT_WARNING_SEVERITY) {
            return;
          }

          const issueJson = buildIssueJson(message, path)
          STDOUT(issueJson + '\u0000\n');
        });
      });

      runWithTiming('gc-batch-' + batchNum, function () {
        batchFiles = null;
        batchReport = null;
        global.gc();
      });

      batchNum++;
    }
  }

  function logInfo() {
    const printList = function (list) {
      const [first, ...rest] = list.sort();
      console.error('\t * ' + first + rest.join('\n\t * '));
    }

    if (debug) {
      console.error('Loaded modules');
      printList(patch.loadedModules());
    }
  }

  // No explicit includes, let's try with everything
  let buildFileList = inclusionBasedFileListBuilder(['./']);

  const changedFiles = process.argv.slice(2)[0];
  if (changedFiles && changedFiles.length > 0) {
    buildFileList = inclusionBasedFileListBuilder(changedFiles.split(','));
  }
  const engineConfig = { userConfig: { configPath: '/usr/src/app/rules/.eslintrc.yaml' } };
  overrideOptions(engineConfig.userConfig);
  cli = new CLIEngine(options);
  const analysisFiles = buildFileList(options.extensions);
  await analyzeFiles(analysisFiles);

  logInfo();
}

module.exports = { run };
